<?php
require('classes/Loader.php');
spl_autoload_register('Loader' . '\Loader::auto_load');

$contacts = new Contacts(Database::database_connection());
$renderer = '';

$id = 0;
$confirm = 0;
if(isset($_GET['id'])) {
    $id = intval($_GET['id']);
}
if(isset($_GET['confirm']) && $_GET['confirm'] == 1 && $_GET['id'] > 0) {
    $result = $contacts->delete_one_contact_from_id($id);
    if($result) {
        header('Location: index.php');
    }
}

$contact_to_delete = $contacts->get_one_contact_from_id($id);
if($contact_to_delete) {
    $renderer .= 'Delete <b>'. $contact_to_delete['first_name'] .' '. $contact_to_delete['last_name'] .'</b> ?<br>';
    $renderer .= HTML::create_delete_buttons($id);
}else {
    header('Location: index.php');
}

echo HTML::create_header();
echo HTML::create_navigation();
echo $renderer;
echo HTML::create_footer();
