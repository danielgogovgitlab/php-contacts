CREATE DATABASE `php-contacts`

CREATE TABLE `php-contacts`.`contacts`( 
    `id` INT NOT NULL AUTO_INCREMENT , 
    `first_name` VARCHAR(255) NOT NULL , 
    `last_name` VARCHAR(255) NOT NULL , 
    `number` INT NOT NULL , 
    `date_created` INT NOT NULL , 
    PRIMARY KEY (`id`)
) ENGINE = InnoDB; 

INSERT INTO `contacts` (`id`, `first_name`, `last_name`, `number`, `date_created`) 
VALUES (NULL, 'Florian', 'Hoffmann', '0669', 1587032110); 
