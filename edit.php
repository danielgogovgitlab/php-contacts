<?php
require('classes/Loader.php');
spl_autoload_register('Loader' . '\Loader::auto_load');

$contacts = new Contacts(Database::database_connection());
$renderer = '';

$contact_id = 0;
if(isset($_GET['id'])) {
    $contact_id = intval($_GET['id']);
}

if($_POST) {
    try {
        $result = $contacts->check_post_input($_POST, 'update', $contact_id);
        if($result) {
            $renderer = 'Contact was updated successfully!<br><br>';
        }else {
            $renderer = 'Please try again later!<br><br>';
        }
    }catch(Exception $exception) {
        $renderer = $exception->getMessage() .' is too short!<br><br>';
    }
}

$current_contact = $contacts->get_one_contact_from_id($contact_id);
if(!$current_contact) {
    header('Location: index.php');
}

echo HTML::create_header();
echo HTML::create_navigation();
echo $renderer;
echo HTML::create_form('edit.php?id='. $contact_id, $current_contact['first_name'], $current_contact['last_name'], $current_contact['number'], 'Update');
