<?php
require('classes/Loader.php');
spl_autoload_register('Loader' . '\Loader::auto_load');

$contacts = new Contacts(Database::database_connection());
$all_contacts_array = $contacts->get_all_contacts();

echo HTML::create_header();
echo HTML::create_navigation();
echo HTML::create_table($all_contacts_array);
echo HTML::create_footer();
