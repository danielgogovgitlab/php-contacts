<?php

class HTML_base {

    /**
     * Constructor
     */
    private function __construct() {

    }

    /**
     * Displays standard header tags
     * 
     * @param string $title
     * @return string
     */
    public static function create_header(string $title = '') : string {
        $header =
'<!DOCTYPE html>
<html lang="en">
        
<!-- Head -->
<head>
    <title>'. $title .'</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<!-- Head -->

<!-- Body -->
<body>
';
        return $header;
    }

    /**
     * Displays standard footer tags
     * 
     * @return string
     */
    public static function create_footer() : string {
        $footer = '
</body>
<!-- Body -->

</html>';
        return $footer;
    }    

}
