<?php

class Contacts {
    
    /**
     * @var database_connection
     */
    private $database;

    /**
     * Constructor
     */    
    public function __construct($database) {
        $this->database = $database;
    }

    /**
     * Get one contact from id
     * 
     * @param int $contact_id
     * @return array|false
     */
    public function get_one_contact_from_id(int $contact_id) {
        $sql = 'SELECT `id`, `first_name`, `last_name`, `number`, `date_created` FROM `contacts` WHERE `id` = '. $contact_id;
        $result = $this->database->query($sql);

        if($result->num_rows === 1) {
            return $result->fetch_assoc();
        }else {
            return false;
        }
    }    
    
    /**
     * Get all contacts
     * 
     * @return array
     */
    public function get_all_contacts() {
        $all_contacts_array = array();
        $sql = 'SELECT `id`, `first_name`, `last_name`, `number`, `date_created` FROM `contacts`';
        $result = $this->database->query($sql);

        if($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $all_contacts_array[$row['id']] = $row;
            }
        }
        
        return $all_contacts_array;
    }
    
    /**
     * Check post input
     * 
     * @param array $post_input
     * @param string $action
     * @param int $contact_id
     * @throws Exception
     * @return bool
     */
    public function check_post_input(array $post_input, string $action, int $contact_id = 0) {
        foreach($post_input as $key => $field) {
            $field = trim($field);
            if(strlen($field) < 3) {
                throw new Exception($key);
            }
        }

        if($action === 'create') {
            return $this->create_new_contact($post_input['first_name'], $post_input['last_name'], $post_input['number']);
        }elseif($action === 'update') {
            return $this->update_one_contact_from_id($contact_id, $post_input['first_name'], $post_input['last_name'], $post_input['number']);
        }
    }

    /**
     * Create new contact
     * 
     * @param string $first_name
     * @param string $last_name
     * @param string $number
     * @return bool
     */
    private function create_new_contact(string $first_name, string $last_name, string $number) : bool {
        $time = time();
        $sql = "INSERT INTO `contacts` (`first_name`, `last_name`, `number`, `date_created`) VALUES ('$first_name', '$last_name', '$number', '$time')";
        return $this->database->query($sql);
    }

    /**
     * Update one contact from id
     * 
     * @param int $contact_id
     * @param string $first_name
     * @param string $last_name
     * @param string $number
     * @return bool
     */
    private function update_one_contact_from_id(int $contact_id, string $first_name, string $last_name, string $number) : bool {
        $sql = "UPDATE `contacts` SET `first_name`= '$first_name', `last_name`= '$last_name' ,`number`= '$number' WHERE `id` = $contact_id";
        return $this->database->query($sql);
    }

    /**
     * Delete one contact from id
     * 
     * @param int $contact_id
     * @return bool
     */
    public function delete_one_contact_from_id(int $contact_id) : bool {
        $contact_to_delete = $this->get_one_contact_from_id($contact_id);
        
        if($contact_to_delete) {
            $sql = 'DELETE FROM `contacts` WHERE `id` = '. $contact_to_delete['id'];
            return $this->database->query($sql); 
        }else {
            return false;
        }
    }    
    
}
