<?php

class HTML extends HTML_base {

    /**
     * Constructor
     */
    private function __construct() {

    }    
    
    /**
     * @param array $all_contacts
     * @return string
     */
    public static function create_table(array $all_contacts) : string {
        $table = '';
        $table .= 
    '<table>
    <tr>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Number</th>
        <th>Created</th>
    </tr>';        
        foreach($all_contacts as $contact) {
            $table .= '<tr><td>'. $contact['first_name'] .'</td><td>'. $contact['last_name'] .'</td><td>'. $contact['number'] .'</td><td>'. date('d-m-Y H:i', $contact['date_created']) .'</td><td><a href="edit.php?id='. $contact['id'] .'">Edit</a></td><td><a href="delete.php?id='. $contact['id'] .'">Delete</a></td></tr>';
        }
        $table .= '</table>';
        
        return $table;     
    }

    /**
     * @param string $form_action
     * @param string $first_name
     * @param string $last_name
     * @param string $number
     * @param string $button_label
     * @return string
     */
    public static function create_form(string $form_action, string $first_name = '', string $last_name = '', string $number = '', string $button_label = 'Create') : string {
        $form = '
    <form action="'. $form_action .'" method="POST">
    <table>
        <tr>
            <td>Firstname</td>
            <td><input type="text" name="first_name" value="'. $first_name .'" required></td>
        </tr>
        <tr>
            <td>Lastname</td>
            <td><input type="text" name="last_name" value="'. $last_name .'" required></td>
        </tr>
        <tr>
            <td>Number</td>
            <td><input type="text" name="number" value="'. $number .'" required></td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" value="'. $button_label .'"></td>
        </tr>
    </table>
    </form>';
        return $form;
    }

    /**
     * @param int $contact_id
     * @return string
     */
    public static function create_delete_buttons(int $contact_id) : string {
        $buttons = '
    <a href="delete.php?id='. $contact_id .'&confirm=1">Yes</a>
    <a href="index.php">No</a>';
        return $buttons;
    }

    /**
     * @return string
     */
    public static function create_navigation() : string {
        $navigation = '
    <div class="navigation">
        <a href="index.php">Home</a>
        |
        <a href="create.php">Create</a>
    </div>
    <hr>';
        return $navigation;
    }
    
}
