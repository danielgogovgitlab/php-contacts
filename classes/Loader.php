<?php
namespace Loader;

class Loader {

    /**
     * Constructor
     */
    private function __construct() {

    }    
    
    /**
     * Auto load
     * 
     * @param string $class
     */
    public static function auto_load($class) {
        require('classes/'. $class .'.php');
    }

}
