<?php

class Database {

    /**
     * Constructor
     */
    private function __construct() {

    }    

    /**
     * Database connection
     */
    public static function database_connection() {
        $server = 'localhost';
        $username = 'root';
        $password = '';
        $database = 'php-contacts';
        return new mysqli($server, $username, $password, $database);
    }
    
}
