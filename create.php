<?php
require('classes/Loader.php');
spl_autoload_register('Loader' . '\Loader::auto_load');

$contacts = new Contacts(Database::database_connection());
$renderer = '';

if($_POST) {
    try {
        $result = $contacts->check_post_input($_POST, 'create');
        if($result) {
            $renderer = 'Contact was created successfully!<br><br>';
        }else {
            $renderer = 'Please try again later!<br><br>';
        }        
    }catch(Exception $exception) {
        $renderer = $exception->getMessage() .' is too short!<br><br>';
    }
}

echo HTML::create_header();
echo HTML::create_navigation();
echo $renderer;
echo HTML::create_form('create.php');
echo HTML::create_footer();
